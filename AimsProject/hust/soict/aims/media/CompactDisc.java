package hust.soict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<Object> {
	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title, String category, float cost) {
		// TODO Auto-generated constructor stub
		super(title, category, cost);
	}
	
	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	public void addTrack(Track track) {
		if (!tracks.contains(track)) {
			tracks.add(track);
		}
	}
	
	public void removeTrack(Track track) {
		if (tracks.contains(track)) {
			tracks.remove(track);
		}
	}
	
	public int getLength() {
		int totalLength = 0;
		
		for (int i = 0; i < tracks.size(); ++i) {
			totalLength += tracks.get(i).getLength();
		}
		
		return totalLength;
	}
	
	@Override
	public void play() {
		for (int i = 0; i < tracks.size(); ++i) {
			System.out.println("Playing DVD: "+ tracks.get(i).getTitle());
			System.out.println("DVD length: "+ tracks.get(i).getLength());
		}
	}
	
	@Override
	public int compareTo(Object obj) {
		CompactDisc disc = (CompactDisc) obj;
		int rs =  this.getTitle().compareTo(disc.getTitle());
		if (rs == 0)
			return Integer.compare(this.getLength(), disc.getLength());
		return rs;
	}
}
