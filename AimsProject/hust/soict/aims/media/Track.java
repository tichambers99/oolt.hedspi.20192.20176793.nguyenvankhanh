package hust.soict.aims.media;

public class Track implements Playable, Comparable<Object> {
	private String title;
	private int length;
	
	public Track() {
		// TODO Auto-generated constructor stub
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
	
	@Override
	public void play() {
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: "+ this.getLength());
	}
	
	@Override
	public int compareTo(Object obj) {
		Track track = (Track) obj;
		int rs = this.getTitle().compareTo(track.getTitle());
		if (rs == 0)
			return Integer.compare(this.getLength(), track.getLength());
		return rs;
	}
}
