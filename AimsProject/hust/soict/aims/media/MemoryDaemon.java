package hust.soict.aims.media;

public class MemoryDaemon implements java.lang.Runnable {

	long memoryUsed = 0;
	
	public MemoryDaemon() {
		// TODO Auto-generated constructor stub
	}
	
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		
		while(true) {
			used = rt.totalMemory() - rt.freeMemory();
			if (used != memoryUsed) {
				System.out.println("\t Memory used = " + used);
				memoryUsed = used;
			}
		}
	}

}
