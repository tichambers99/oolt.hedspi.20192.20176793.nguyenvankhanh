package hust.soict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable<Object> {
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;
	
	public Book(String title, String category, float cost, List<String> authors) {
		// TODO Auto-generated constructor stub
		super(title, category, cost);
		this.authors = authors;
	}

	public List<String> getAuthors() {
		return authors;
	}
	
	public void addAuthor(String authorName) {
		if (!authors.contains(authorName))
			authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		if (authors.contains(authorName))
			authors.remove(authorName);
	}
	
	@Override
	public int compareTo(Object obj) {
		Book book = (Book) obj;
		return this.getTitle().compareTo(book.getTitle());
	}
	
	public void processContent() {
		this.contentTokens = Arrays.asList(this.content.split("  ,.?!"));
		Collections.sort(this.contentTokens);
		
		for (String token : contentTokens) {
			
		}
	}
	
	
	
	
}
