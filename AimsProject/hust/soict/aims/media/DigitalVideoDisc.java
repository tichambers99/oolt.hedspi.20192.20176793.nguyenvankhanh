package hust.soict.aims.media;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Object> {

	private String director;

	private int length;

	/**
	 * Constructor for DigitalVideoDisc
	 */
	public DigitalVideoDisc(String title, String category, float cost) {
		super(title, category, cost);
	}

	/**
	 * @return Returns the director.
	 */
	public String getDirector() {
		return director;
	}

	/**
	 * @param director
	 *            The director to set.
	 */
	public void setDirector(String director) {
		this.director = director;
	}

	/**
	 * @return Returns the length.
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            The length to set.
	 */
	public void setLength(int length) {
		this.length = length;
	}
	
	// override
	public void play() {
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: "+ this.getLength());
	}
	
	@Override
	public int compareTo(Object obj) {
		DigitalVideoDisc disc = (DigitalVideoDisc) obj;
		return this.getDirector().compareTo(disc.getDirector());
	}
}