package hust.soict.aims.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Aims {

	public Aims() {
		super();
	}
	
	public static void showMenu() {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Order Managment Application.");
		System.out.println("============================");
		System.out.println("1. Create new order.");
		System.out.println("2. Add item to order.");
		System.out.println("3. Delete items by id.");
		System.out.println("4. Display the items list of orders.");
		System.out.println("0. Exit.");
		System.out.println("===========================");
		System.out.println("Please choose a number: 0-1-2-3-4 ");
		
		int choice = scan.nextInt();
		
		if (choice == 1) {
		
		}
		else if (choice == 2) {
			System.out.println("Which type you want to add?");
			System.out.println("============================");
			System.out.println("1. Book.");
			System.out.println("2. Compact Disc.");
			System.out.println("3. Digital Video Disc.");
			System.out.println("============================");
			System.out.println("Please choose a number: 1-2-3.");
		}
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// Create a memory daemon 
		// Create a new Order object

		Order anOrder = new Order();

		// Create a new dvd object and set the fields
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation", 19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);

		// add the dvd to the order
		anOrder.addMedia(dvd1);

		// Create a new dvd object and set the fields
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f);
		dvd2.setDirector("George Lucas");
		dvd2.setLength(124);

		//add the dvd to the order
		anOrder.addMedia(dvd2);

		// Create a new dvd object and set the fields
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", 18.99f);
		dvd3.setDirector("John Musker");
		dvd3.setLength(90);

		// add the dvd to the order
		anOrder.addMedia(dvd3);

		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());
		
		// add to collection but unorder
		Collection<DigitalVideoDisc> collection = new ArrayList<DigitalVideoDisc>();
		collection.add(dvd2);
		collection.add(dvd1);
		collection.add(dvd3);
		
		Iterator<DigitalVideoDisc> iterator = collection.iterator();
		
		System.out.println("===============================================");
		System.out.println("The DVDs currently in the order are: ");
		
		while (iterator.hasNext()) {
			System.out.println(( (DigitalVideoDisc)iterator.next() ).getTitle());
		}
		
		// order
		Collections.sort((List<DigitalVideoDisc>)collection);
		
		iterator = collection.iterator();
		
		System.out.println("===============================================");
		System.out.println("The DVDs in sorted order are: ");
		
		while (iterator.hasNext()) {
			System.out.println(( (DigitalVideoDisc)iterator.next() ).getTitle());
		}
			
		
		
	}
}