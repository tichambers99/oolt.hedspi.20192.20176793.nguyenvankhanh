package hust.soict.aims.media;

import java.util.ArrayList;

public class Order {

	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	/**
	 *  
	 */
	public Order() {
		super();
	}
	
	public void addMedia(Media media) {
		if( !itemsOrdered.contains(media) ) {
			itemsOrdered.add(media);
		}
	}
	
	public void removeMeida(Media media) {
		if( itemsOrdered.contains(media) ) {
			itemsOrdered.remove(media);
		}
	}

	public float totalCost() {
		// store the running total of the discs in the order
		float total = 0;

		// loop through the discs in the order
		for (int i = 0; i < itemsOrdered.size(); i++) {
			total = total + itemsOrdered.get(i).getCost();
		}
		return total;
	}
}