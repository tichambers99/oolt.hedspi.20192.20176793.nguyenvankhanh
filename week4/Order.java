
public class Order {
	private int qtyOrdered;
	public static final int MAX_NUMBER_ORDER=11;
	private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc[MAX_NUMBER_ORDER];
	private MyDate dateOrderd = new MyDate();

	public static final int MAX_LIMITTED_ORDERS = 5;
	private static int nbOrders = 0;
	
	public Order() {
		super();
		if(nbOrders <= MAX_LIMITTED_ORDERS) nbOrders++;
		else {
			System.out.println(" You have " + MAX_LIMITTED_ORDERS+", you can't push order.");
		}
		this.qtyOrdered = 0;
	}
	
	public Order(DigitalVideoDisc[] itemsOrdered, MyDate dateOrderd) {
		super();
		this.itemsOrdered = itemsOrdered;
		this.dateOrderd = dateOrderd;
	}

	public MyDate getDateOrderd() {
		return dateOrderd;
	}

	public void setDateOrderd(MyDate dateOrderd) {
		this.dateOrderd = dateOrderd;
	}

	/*public Order() {
		super();
		this.qtyOrdered=0;
	}*/
	
	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	public void addDigitalVideoDisc(DigitalVideoDisc disc){
		if (this.qtyOrdered<MAX_NUMBER_ORDER){
			this.itemsOrdered[qtyOrdered+1]=disc;
			this.qtyOrdered=this.qtyOrdered+1;
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc[] dvdList ) {
		if(this.qtyOrdered < MAX_NUMBER_ORDER) {
			if(this.qtyOrdered + dvdList.length < MAX_NUMBER_ORDER) {
				for(int i = 0; i < dvdList.length; i++) {
					this.itemsOrdered[qtyOrdered+1] = dvdList[i];
					this.qtyOrdered = this.qtyOrdered + 1;
				}
			} else {
				System.out.println("Over the maximum of Order");
			}
		} else {System.out.println("Over the maximum of Order");
			}
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc){
		int j=0;
			for (int i=1;i<=this.qtyOrdered;i++){
				if (itemsOrdered[i].getTitle().equals(disc.getTitle())){
					j=1;
					this.qtyOrdered=this.qtyOrdered-1;
				}
				if (j==1){
					itemsOrdered[i]=itemsOrdered[i+1];
				}
			}
	}
	public void totalCost(){
		float tong=0;
		for (int i=1;i<=this.qtyOrdered;i++){
			tong=tong+itemsOrdered[i].getCost();
		}
		System.out.println(tong);
	
	}
	
	public void printOrder() {
		System.out.println("*********************Order************************");
		System.out.print("Date: ");
		dateOrderd.print();
		System.out.println("Ordered Items:");
		/*System.out.println("Date:" + this.getDateOrderd());*/
		/*for( int i = 1; i <= this.qtyOrdered; i++) {
			System.out.println(i + "." +itemsOrdered[i].getTitle()+itemsOrdered[i].getCategory()+itemsOrdered[i].getDirector()+itemsOrdered[i].getLength()+":"+itemsOrdered[i].getCost() +"$");
		}*/
		for(int i = 1; i<=this.qtyOrdered; i++) {
			System.out.println(i+"-DVD-"+itemsOrdered[i].getTitle()+"-"+itemsOrdered[i].getCategory()+"-"+itemsOrdered[i].getDirector()+"-"+itemsOrdered[i].getLength()+":"+itemsOrdered[i].getCost()+"$"    );
		}
			System.out.print("Total Cost: "); this.totalCost();
			System.out.println("**************************************************");
	
		
	}
	
}
