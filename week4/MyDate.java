/*import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	public MyDate() {
		super();
	}
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String str) {
		super();
		int count = 0;
		char[] list = str.toCharArray();
		String date = "";
		for(int i = 0; i<list.length; i++) {
			count++;
			date+=list[i];
			if(list[i] == ' '){
				break;
			}
		}
		switch(date.trim()) {
			case "January": case "1": case "Jan":
				this.setMonth(1); break;
			case "February": case "2": case "Feb":
				this.setMonth(2); break;
			case "March": case "3": case "Mar":
				this.setMonth(3); break;
			 case "April": case "4": case "Apr":
				 this.setMonth(4); break;
			case "May": case "5": 
				 this.setMonth(5); break;
			case "June": case "Jun": case "6":
				 this.setMonth(6); break;
			case "July": case "Jul": case "7":
				this.setMonth(7); break;
		      case "August": case "8": case "Aug":
		    	  this.setMonth(8); break;
		      case "September": case "9": case "Sep":
		    	  this.setMonth(9); break;
		      case "October": case "10": case "Oct":
		    	  this.setMonth(10); break;
		      case "November": case "11": case "Nov":
		    	  this.setMonth(11); break;
		      case "December": case "12": case "Dec":
		    	  this.setMonth(12); break;
		      default:
		    	  this.setMonth(0);	
		}
		if(this.getMonth()!=0) {
			date = "";
			for(int i = list.length-1; i>=0; i--) {
				date+=list[i];
				if(list[i]==' ') {
					break;
				}
			}
			date = new StringBuilder(date).reverse().toString();
			this.setYear(Integer.parseInt(date.trim()));
			date = "";
			char[] list1 = str.substring(count).trim().toCharArray();
			String testDate = "";
			for(int i = 0; i<list1.length; i++) {
				if(list1[i]==' ') {
					break;
				}
				if(list1[i]>='0' && list1[i]<='9') {
					date+=list1[i];
				}else {
					testDate+=list1[i];
				}
			}
			System.out.print(testDate);
			this.setDay(Integer.parseInt(date.trim()));	
			if(this.getDay()!=0) {
				if(testDate.equals("st")) {
					if(this.getDay() == 1 || this.getDay() == 21 || this.getDay() == 31) {
						print();
					}
					else {
						System.out.println("Dinh dang ngay ko hop le. Nhap lai.");
						accept();
					}
				}else if(testDate.equals("nd")) {
					if(this.getDay() == 2 || this.getDay() == 22) {
						print();
					}else {
						System.out.println("Dinh dang ngay ko hop le. Nhap lai.");
						accept();
					}
				}else if(testDate.equals("rd")) {
					if(this.getDay() == 3 || this.getDay() == 23) {
						print();
					}else {
						System.out.println("Dinh dang ngay ko hop le. Nhap lai.");
						accept();
					}
				}else if(testDate.equals("th")){
					if(this.getDay() != 1 || this.getDay() != 21 || this.getDay() != 31 || 
							this.getDay() != 2 || this.getDay() != 22 ||
							this.getDay() != 3 || this.getDay() != 23) {
						print();
					}else {
						System.out.println("Dinh dang ngay ko hop le. Nhap lai.");
						accept();
					}
				}else {
					System.out.println("Dinh dang ngay ko hop le. Nhap lai.");
					accept();
				}
				
			}
		}
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if(day<1 || day >31) {
			System.out.println("Ngay khong hop le. Nhap lai.");
			accept();
		}else {
			if(this.getMonth() == 1|| this.getMonth() == 3|| this.getMonth() == 5
					||	this.getMonth() == 7|| this.getMonth() == 8|| this.getMonth() ==10
					||	this.getMonth() == 12){
					this.day = day;
				}
				if(this.getMonth() == 2) {
					if(day>29) {
						System.out.println("Ngay khong hop le. Nhap lai");
						accept();
					}
					else if(day == 29 && this.getYear()%4!=0) {
						System.out.println("Ngay khong hop le. Nhap lai");
						accept();
					}
					else {
						this.day = day;
					}
				}
				if(this.getMonth() == 4||this.getMonth() == 6|| this.getMonth() == 9||
						this.getMonth() == 11) {
					if(day == 31) {
						System.out.println("Ngay khong hop le. Nhap lai.");
						accept();
					}
					else{
						this.day = day;
					}
				}
		}
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if(month>=1 && month <=12) {
			this.month = month;
		}else {
			System.out.println("Thang khong hop le. Nhap lai");
			accept();
		}
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if(year >= 0) {
			this.year = year;
		}else {
			System.out.println("Nam khong hop le. Nhap lai");
			accept();
		}
	}
	public static void accept(){
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap: ");
		String str1 = "";
		String str = "\\w+\\s\\d{1,2}[strndh]{2}\\s\\d+";
		do {
			str1 = sc.nextLine();
			if(!str1.matches(str)) {
				System.out.println("Nhap sai. Nhap lai");
				System.out.print("Nhap: ");
			}
		}while(!str1.matches(str));
		MyDate md = new MyDate(str1);
	}
	public void print() {
		System.out.print("Current day is: "+this.getDay()+"/"+this.getMonth()+"/"+this.getYear());
	}
}*/


/*import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;
import java.util.TimeZone;

public class MyDate {
	private int day;
	private int month;
	private int year;
	private String mydate;
	
    Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
    //getTime() returns the current date in default time zone
    Date date = calendar.getTime();
    private int curr_day = calendar.get(Calendar.DATE);
    //Note: +1 the month for current month
    private int curr_month = calendar.get(Calendar.MONTH) + 1;
    private int curr_year = calendar.get(Calendar.YEAR);
    

	public int getDay() {
		return day;
	}

	public int setDay(int day) {
		if(0 >= day || day > 31)
		{
			System.out.println("inserted day is incompatible");
			return 1;
		}
		
		this.day = day;
		return 0;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int setMonth(int month) {
		if(0 >= month || month > 12)
		{
			System.out.println("inserted month is incompatible");
			return 1;
		}
		this.month = month;
		
		return 0;
	}
	
	public int getYear() {
		return year;
	}
	
	public int setYear(int year) {
		if(0 >= year)
		{
			System.out.println("inserted year is incompatible");
			return 1;
		}
		
		this.year = year;
		return 0;
	}
	
	public void accept()
	{
		Scanner sc = new Scanner(System.in);
		int day, month, year;
		int check;
		
		do {
			System.out.print("Enter day:  "); day = sc.nextInt();
			check = this.setDay(day);
		}while(check == 1);
		
		do {
			System.out.print("Enter month: "); month = sc.nextInt();
			check = this.setMonth(month);
		}while(check == 1);
		
		do {
			System.out.print("Enter year: "); year = sc.nextInt();
			check = this.setYear(year);
		}while(check == 1);
		
		this.mydate = Integer.toString(this.curr_day) +"//" + Integer.toString(this.curr_month) + "//" + Integer.toString(this.curr_year);
	}
	
	public void print()
	{
		System.out.println(this.mydate);
	}
	
}*/


import java.util.Calendar;

import javax.swing.JOptionPane;

public class MyDate {
    private int date;
    private int month;
    private int year;
    
	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
    
	public MyDate() {
		Calendar cal = Calendar.getInstance(); 
		setDate(cal.get(Calendar.DAY_OF_MONTH));
		setMonth(cal.get(Calendar.MONTH));
		setYear(cal.get(Calendar.YEAR));
	}
	
	
	
    public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
	}
    
    public void Input() {
    	String strDate,strMonth,strYear;
    	int date,month,year;
    	do {
    		strDate = JOptionPane.showInputDialog(null,"nhap ngay",JOptionPane.INFORMATION_MESSAGE);
    		date = Integer.parseInt(strDate);
    		setDate(date);
    	}while(date < 0 || date > 32);
    	
    	do {
    		strMonth = JOptionPane.showInputDialog(null,"nhap thang",JOptionPane.INFORMATION_MESSAGE);
    		month = Integer.parseInt(strMonth);
    		setMonth(month);
    	}while(month > 12 || month < 0);
    	
    	do {
    		strYear = JOptionPane.showInputDialog(null,"nhap nam",JOptionPane.INFORMATION_MESSAGE );
    		year = Integer.parseInt(strYear);
    		setYear(year);
    	}while(year <0 );
    	
    }
    
   

    public void GetDateFromUser() {
    	String strDate,strMonth,strYear, str;
    	String [] s;
    	int date,month,year;
    	str = JOptionPane.showInputDialog(null,"Input Date",JOptionPane.INFORMATION_MESSAGE);
    	s = str.split(" ");
    	strDate = s[1];
    	strMonth = s[0];
//    	strYear = s[2] +s[3];
    	int s1= convertYear(s[2]);
    	int s2 = convertYear(s[3]);
    	date = convertDate(strDate);setDate(date);
    	month = convertDate(strMonth);setDate(month);
    	year = s1*100 + s2;setYear(year);
    }
    
    public void showDateOnScreen() {
    	System.out.println(this.getDate()+"/"+this.getMonth()+"/"+this.getYear());
    }
    
    public void showDate() {
    	JOptionPane.showMessageDialog(null, "Ngay/Thang/Nam :" + getDate()+ "/" +getMonth()+ "/" +getYear() );
    }
    
    public int convertYear(String s) {
    	int date;
    	String str = s.toLowerCase();
    	switch (str) {
    	case "zero":date =0;break;
    	case "one": date =  1;break;
		case "two": date =  2;break;
        case "three": date = 3;break;
		case "four":  date = 4; break;
		case "five":  date = 5; break;
		case "six":  date = 6; break;
		case "seven":  date = 7; break;
		case "eight":  date = 8; break;
		case "nine":  date = 9; break;
		case "ten":  date = 10; break;
		case "eleven":  date = 11; break;
		case "twelve":  date = 12; break;
		case "thirteen":  date = 13; break;
		case "fourteen":  date = 14; break;
		case "fifteen":  date = 15; break;
		case "sixteen":  date = 16; break;
		case "seventeen":  date = 17; break;
		case "eighteen":  date = 18; break;
		case "nineteen":  date = 19; break;
		case "twenty":  date = 20; break;
		default:
			date = 0;
			break;
		}
		return date;
    }
    public int convertDate(String s) {
    	int date;
    	String str = s.toLowerCase();
    	switch (str) {
		case "first": date =  1;break;
		case "second": date =  2;break;
        case "third": date = 3;break;
		case "forth":  date = 4; break;
		case "fifth":  date = 5; break;
		case "sixth":  date = 6; break;
		case "seventh":  date = 7; break;
		case "eighth":  date = 8; break;
		case "ninth":  date = 9; break;
		case "tenth":  date = 10; break;
		case "eleventh":  date = 11; break;
		case "twelfth":  date = 12; break;
		case "thirteenth":  date = 13; break;
		case "fourteenth":  date = 14; break;
		case "fifteenth":  date = 15; break;
		case "sixteenth":  date = 16; break;
		case "seventeenth":  date = 17; break;
		case "eighteenth":  date = 18; break;
		case "nineteenth":  date = 19; break;
		case "twentieth":  date = 20; break;
		case "twenty-first":  date = 21; break;
		case "twenty-second":  date = 22; break;
		case "twenty-third":  date = 23; break;
		case "twenty-fourth": date = 24;break;
		case "twenty-fifth" : date = 25;break;
		case "twenty-sixth":date = 26;break;
		case "twenty-seventh":date = 27;break;
		case "twenty-eighth": date = 28;break;
		case "twenty-ninth":date = 29;break;
		case "thirtieth":date = 30;break;
		case "thirty-first":date = 31;break;
		case "january": date = 1; break;
		case "february": date = 2; break;
		case "march": date = 3;break;
		case "april":  date = 4; break;
		case "may":  date = 5; break;
		case "june":  date = 6; break;
		case "junly":  date = 7; break;
		case "august":  date = 8; break;
		case "september":  date = 9; break;
		case "october":  date = 10; break;
		case "november":  date = 11; break;
		case "decemver":  date = 12; break;
		default:
			date = 0;
			break;
		}
		return date;
    }
    
    public void print() {
    	System.out.print("Current day is: "+this.getDate()+"/"+this.getMonth()+"/"+this.getYear());
    }
}
