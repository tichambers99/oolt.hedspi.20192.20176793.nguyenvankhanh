package hust.soict.ictglobal.aims.media;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.order.Date.MyDate;
import hust.soict.ictglobal.aims.order.Order.Order;

public class DiskTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
			// TODO Auto-generated method stub
			Order anOrder1 = new Order();
			Order anOrder2 = new Order();
	        
	        DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	        dvd1.setCategory("Animation");
	        dvd1.setCost(19.95f);
	        dvd1.setDirector("Roger Allers");
	        dvd1.setLength(87);
            System.out.println(dvd1.search("king"));
	        anOrder1.addDigitalVideoDisc(dvd1);

	        DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
	        dvd2.setCategory("Science Fiction");
	        dvd2.setCost(24.95f);
	        dvd2.setDirector("George Lucas");
	        dvd2.setLength(124);

	        anOrder1.addDigitalVideoDisc(dvd2);

	        DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
	        dvd3.setCategory("Anamation");
	        dvd3.setCost(18.99f);
	        dvd3.setDirector("Join Musker");
	        dvd3.setLength(90);
	        anOrder1.setDateOrderd(new MyDate(13,11,1999));

	        anOrder1.addDigitalVideoDisc(dvd3);
	        anOrder1.printItemOfOrders();
	        anOrder2.addDigitalVideoDisc(dvd1, dvd2);
	        anOrder2.setDateOrderd(new MyDate(2,11,1999));
	        
	        anOrder1.addDigitalVideoDisc(anOrder1.getALuckyItem());
	        anOrder1.printItemOfOrders();
	
	}

}
