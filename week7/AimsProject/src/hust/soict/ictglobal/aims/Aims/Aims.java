package hust.soict.ictglobal.aims.Aims;

import java.util.Scanner;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.CompactDisc;
import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.order.Order.Order;

public class Aims {
	public static void main(String[] args) {
		Order o = null;
		int choiceNumber;
		CompactDisc c = null;
		Scanner sc =  new Scanner(System.in);
			do {
				showMenu();
				choiceNumber = sc.nextInt();
				switch (choiceNumber) {
					case 1:
						o = new Order();
						System.out.println("You have been created a new order! Please add item in order!");
						break;
					case 2:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							System.out.println("Book(1) or Disc(2) or CompactDisc(3)");
							int type = sc.nextInt();
							if(type == 1) {
								System.out.println("How many Items you want to add?");
								int n = sc.nextInt();
								Book [] b = new Book[n];
								o.getInforBooks(b);
								o.addMediaList(b);
								System.out.println("Add item in the order successfull!");
							}
							else if(type == 2) {
								System.out.println("How many Items you want to add?");
								int n = sc.nextInt();
								DigitalVideoDisc [] dvd = new DigitalVideoDisc[n];
								o.getInforDiscs(dvd);
								o.addMediaList(dvd);
								System.out.println("Add item in the order successfull!");
							}else if(type==3) {
								//todo
								 c = new CompactDisc();
								System.out.println("How many track you want to add?");
								int n = sc.nextInt();
							    c.setTracks(n);
								System.out.println("Add track in the order successfull!");
								
							}
							else {
								System.out.println("Invalid type!");
							}
						}
						break;
					case 3:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							System.out.println("Input the id that you want to delete from Order?");
							int id = sc.nextInt();
							o.removeMediaByID(id);
						}
						break;
					case 4:
						if(o == null) {
							System.out.println("You didn't created an order, CREATE now!");
						}else {
							o.showOrder();
						}
						break;
					case 5: c.play();
						
					case 0:
						System.out.println("See you again");
						System.exit(0);
				}
			}while(choiceNumber != 0);
		
	}
	
	public static void showMenu() {
		System.out.println("\n\nOrder Management Application: ");
		System.out.println("-----------------------------------");
		System.out.println("1.Create order");
		System.out.println("2.Add item to the order");
		System.out.println("3.Delete item by id");
		System.out.println("4.Display the items list of order");
		System.out.println("5.Play track in the tracks");
		System.out.println("0.Exit");
		System.out.println("-----------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4-5");
	}
	

}
