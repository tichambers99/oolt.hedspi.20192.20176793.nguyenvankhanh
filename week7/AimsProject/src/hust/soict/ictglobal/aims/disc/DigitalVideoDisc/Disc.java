package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import hust.soict.ictglobal.aims.media.Media;

public class Disc extends Media {
	private String director;
	private int length;
	public Disc() {
		// TODO Auto-generated constructor stub
		
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	

}
