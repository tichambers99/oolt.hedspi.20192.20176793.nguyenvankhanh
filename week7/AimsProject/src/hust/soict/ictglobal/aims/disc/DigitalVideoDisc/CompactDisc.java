package hust.soict.ictglobal.aims.disc.DigitalVideoDisc;

import java.util.ArrayList;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable{
    private String artist;
    private int length;
    private ArrayList<Track>tracks = new ArrayList<Track>();
	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	// kiem tra input da ton tai chua neu roi thong bao cho user
   public void addTrack(Track s) {
	   
		   if(tracks.contains(s)) {
			   System.out.println("Track is already in the list of tracks");
		   }else {
			   tracks.add(s);
			   System.out.println("Track is just added.");
		   }
	   
   }
   // kiem tra input co trong list ch�a ko co thong bao cho user
   public void removeTrack(Track s) {
	   if(tracks.contains(s)) {
		   tracks.remove(s);
		   System.out.println("Track is just removed.");
	   }else {
		   System.out.println("Track isn't exist");
	   }
   }
   public int getLength() {
	   this.length = tracks.size();
	   return this.length;
   }
	// With track
	public void setTrack() {
		Scanner sc =  new Scanner(System.in);
		System.out.println("You input title track:");
		String s = sc.nextLine();
		
		System.out.println("You input length of track:");
		int count = sc.nextInt();
	    Track  m = new Track(s,count);
	    this.addTrack(m);
	    
	}
	


	public void setTracks(int n ) {
		Scanner sc =  new Scanner(System.in);
		System.out.println("You input name artist: ");
		String s = sc.nextLine();
		this.artist = s;
		for(int i =0;i<n;i++) {
			System.out.println("You input title track"+i);
			this.setTrack();
			
			
		}
	}
	
@Override
public void play() {
	// TODO Auto-generated method stub
	System.out.println("Artist:"+this.getArtist());
	System.out.println("Length of tracks:"+this.getLength());
	for(int i = 0;i<this.length;i++) {
		tracks.get(i).play();
	}
	
}
}
