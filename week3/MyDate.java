package homeWork;

import java.util.Scanner;

public class MyDate {
	private int date;
	private int month;
	private int year;
	public MyDate() {
		super();
	}
	public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
	}

	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public void nhap(){
		int date,month,year;
		System.out.print("Nhap ngay : ");
		date=new Scanner(System.in).nextInt();
		System.out.print("Nhap thang : ");
		month=new Scanner(System.in).nextInt();
		System.out.print("Nhap nam : ");
		year=new Scanner(System.in).nextInt();
		this.date=date;
		this.month=month;
		this.year=year;
		System.out.println("ngay "+this.date+"/ thang "+this.month+"/ nam "+this.year);
	}
}
