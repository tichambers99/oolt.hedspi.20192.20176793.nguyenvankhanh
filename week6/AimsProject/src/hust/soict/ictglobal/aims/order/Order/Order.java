package hust.soict.ictglobal.aims.order.Order;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import hust.soict.ictglobal.aims.disc.DigitalVideoDisc.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.order.Date.MyDate;

//import java.util.Scanner;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMIT_ORDERS = 5;
	private static int nbOrders = 0;
	private ArrayList<Media> itemsOrdered =  new ArrayList<Media>();
	private MyDate dateOrdered;
	

	public Order () {
		super();
		dateOrdered = new MyDate();
		dateOrdered.GetDateFromUser();
		if(nbOrders <= MAX_LIMIT_ORDERS) Order.setNbOrders(Order.getNbOrders() + 1);
		else System.out.println("You have " + MAX_LIMIT_ORDERS + "orders, so can't order now!");
	}
	

	
	public static int getNbOrders() {
		return nbOrders;
	}


	public static void setNbOrders(int nbOrders) {
		Order.nbOrders = nbOrders;
	}
	
	public void addMedia(Media m) {
		if(this.itemsOrdered.size() >= MAX_NUMBERS_ORDERED) {
			System.out.println("The order is full! Can't add " + m.getTitle());
		}
		else {
			itemsOrdered.add(m);
		}
	}
	
	
	public void removeMedia(Media m) {
		for(int i = 0; i < this.itemsOrdered.size(); i++) {
			if(itemsOrdered.indexOf(m) == i) {
				itemsOrdered.remove(i);
				System.out.println("Remove " + m.getTitle() + " successfull!");
				return;	
			}
		}
		System.out.println("not exist!");
	}
	
	public void removeMediaByID(int k) {
		this.removeMedia(itemsOrdered.get(k-1));
	}
	
	public float totalCost() {
		float cost = 0;
		for(int i = 0 ; i < this.itemsOrdered.size(); i ++) {
			cost += itemsOrdered.get(i).getCost();
		}
		return cost;
	}
	
	public void addMediaList(Media [] mList) {
		if(this.itemsOrdered.size() + mList.length <= MAX_NUMBERS_ORDERED) {
			for(int i = 0; i <  mList.length; i ++) {
				this.addMedia(mList[i]);
			}
		}
		else System.out.println("\nThe list's size is over the size of dics, can't add!");
	}
	
	public void addMedia(Media m1, Media m2) {
		this.addMedia(m1);
		this.addMedia(m2);
	}

	static Random rand = new Random();
	static final String AB = "ABCDE fghijkLMNOPQRS tuvwxyz";
	public static String randomString( int len ){
	       StringBuilder sb = new StringBuilder( len );
	       for( int i = 0; i  < len; i++ ) 
	          sb.append( AB.charAt( rand.nextInt(AB.length()) ) );
	       return sb.toString();
	}
	
	public DigitalVideoDisc getALuckyItemDigital() {
	    DigitalVideoDisc dvd = new DigitalVideoDisc();
		dvd.setTitle(randomString(20));
		dvd.setCategory(randomString(10));
		dvd.setDirector(randomString(15));
		dvd.setLength(rand.nextInt(200));
		dvd.setCost((float)rand.nextInt(100));
		return dvd;
	}
	
	public Book getALuckyItemBook() {
	    Book b = new Book();
		b.setTitle(randomString(20));
		b.setCategory(randomString(10));
		List<String> a = new ArrayList<String>();
		a.add(randomString(10));
		b.setAuthors(a);
		b.setCost((float)rand.nextInt(100));
		return b;
	}
	
	public void getInforDiscs(DigitalVideoDisc [] dvdList) {
		for(int i = 0; i < dvdList.length; i ++) {
			dvdList[i] = this.getALuckyItemDigital();
		}
	}
	
	public void getInforBooks( Book [] bList) {
		for(int j = 0; j < bList.length; j ++) {
			bList[j] = this.getALuckyItemBook();
		}
	}
	
	public void showListDicsOfOder() {
		for(int i = 0; i < this.itemsOrdered.size(); i++) {
			System.out.print(i + 1 + "-");
			System.out.print(itemsOrdered.get(i).getTitle() +" - ");
			System.out.print(itemsOrdered.get(i).getCategory()+" - ");
			System.out.print(((DigitalVideoDisc) itemsOrdered.get(i)).getDirector()+" - ");
			System.out.print(((DigitalVideoDisc) itemsOrdered.get(i)).getLength()+" : ");
//			for (String s : ((Book) itemsOrdered.get(i)).getAuthors() ) {
//	            System.out.print(s);
//	        }
			System.out.println(itemsOrdered.get(i).getCost()+" $ ");
		}
	}
	
	public void showListBooksOfOder() {
		for(int i = 0; i < this.itemsOrdered.size(); i++) {
			System.out.print(i + 1 + "-");
			System.out.print(itemsOrdered.get(i).getTitle() +" - ");
			System.out.print(itemsOrdered.get(i).getCategory()+" - ");
			for (String s : ((Book) itemsOrdered.get(i)).getAuthors() ) {
	            System.out.print(s);
	        }
			System.out.println("-" + itemsOrdered.get(i).getCost()+" $ ");
		}
	}
	
	
	public void showOrder() {
		System.out.println("**************************************order**********************************");
		System.out.print("Date: ");
		this.dateOrdered.showDateOnScreen();
		this.showListDicsOfOder();
		System.out.println("\nTotal cost: "+this.totalCost() + '$');
		System.out.println("******************************************************************************\n");
	}
	
}
