package hust.soict.ictglobal.aims.order.Date;


import javax.swing.JOptionPane;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class MyDate {
  private int date;
  private int month;
  private int year;
  
	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
  
//	public MyDate() {
//		Calendar cal = Calendar.getInstance(); 
//		setDate(cal.get(Calendar.DAY_OF_MONTH));
//		setMonth(cal.get(Calendar.MONTH));
//		setYear(cal.get(Calendar.YEAR));
//	}
	
	public MyDate() {
		super();
	}
	
	
  public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
	}
  
//  public void Input() {
//  	String strDate,strMonth,strYear;
//  	int date,month,year;
//  	do {
//  		strDate = JOptionPane.showInputDialog(null,"nhap ngay",JOptionPane.INFORMATION_MESSAGE);
//  		date = Integer.parseInt(strDate);
//  		setDate(date);
//  	}while(date < 0 || date > 32);
//  	
//  	do {
//  		strMonth = JOptionPane.showInputDialog(null,"nhap thang",JOptionPane.INFORMATION_MESSAGE);
//  		month = Integer.parseInt(strMonth);
//  		setMonth(month);
//  	}while(month > 12 || month < 0);
//  	
//  	do {
//  		strYear = JOptionPane.showInputDialog(null,"nhap nam",JOptionPane.INFORMATION_MESSAGE );
//  		year = Integer.parseInt(strYear);
//  		setYear(year);
//  	}while(year <0 );
//  	
//  }
  
  
  public int checkDate(int a) {
  	if(a == 0) return 0;
  	if(this.getMonth() == 2) {
  		if(this.checkNhuan(this.getYear()) == 1) {
  			if(a == 29) return 1;
  			else return 0;
  		}
  		else {
  			if(a == 28) return 1;
  			else return 0;
  		}
  	}
  	else return 1;
  }
  
  public int checkYear(int a) {
  	if(a>=10000) return 0;
  	else return 1;
  }
  
  public int checkMonth(int a) {
  	if(a == 0) return 0;
  	else return 1;
  }
  
  public int checkNhuan(int year) {
  	 if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)) {
  		 return 1;
       } else {
           return 0;
       }
  }
  
  public int checkAllDateInput() {
  	int a = this.checkDate(this.getDate());
  	int b = this.checkMonth(this.getMonth());
  	int c = this.checkYear(this.getYear());
      	if(a == 0 && b == 1 && c == 1) {
      		System.out.println("invalid Date");
//      		JOptionPane.showMessageDialog(null,"Invalid Date");
      		return 0;
      	}
      	else if(b == 0 && a == 1 && c == 1) {
      		System.out.println("invalid Month");
//      		JOptionPane.showMessageDialog(null,"Invalid Month");
      		return 0;
      	}
      	else if(c == 0 && a == 1 && b == 1) {
      		System.out.println("invalid Year");
//      		JOptionPane.showMessageDialog(null,"Invalid Year");
      		return 0;
      	}
      	else if(a == 0 && b == 0 && c == 1) {
      		System.out.println("invalid Date and Month");
//      		JOptionPane.showMessageDialog(null,"Invalid Date and Month");
      		return 0;
      	}
      	else if(a == 0 && c == 0 && b == 1) {
      		System.out.println("invalid Date and Year");
//      		JOptionPane.showMessageDialog(null,"Invalid Date and Year");
      		return 0;
      	}
      	else if(b == 0 && c == 0 && b == 1) {
      		System.out.println("invalid Year and Month");
//      		JOptionPane.showMessageDialog(null,"Invalid Month and Year");
      		return 0;
      	}
      	else if(b == 0 && c == 0 && c == 0) {
      		System.out.println("Invalid all Item");
//      		JOptionPane.showMessageDialog(null,"Invalid all Item");
      		return 0;
      	}
      	else return 1;
    }
  Scanner input = new Scanner(System.in);
  
	public void GetDateFromUser() {
  	String strDate,strMonth,str;
  	String [] s;
  	int date,month,year;
  	int ck = 0;
  	int s1, s2;
  	do {
  		System.out.println("Input Date by Form (Day Month Year example:first may twenty nine)");
//	    	str = JOptionPane.showInputDialog(null,"Input Date by Form (Day Month Year)",JOptionPane.INFORMATION_MESSAGE);
  		str = input.nextLine();
	    	s = str.split(" ");
	    	if(s.length != 4) {
	    		JOptionPane.showMessageDialog(null,"Date's information isn't enough feilds");
	    		ck = 0;
	    	}
	    	else {
		    	strDate = s[0];
		    	strMonth = s[1];
		    	s1 = convertYear(s[2]);
		    	s2 = convertYear(s[3]);
		    	date = convertDate(strDate);setDate(date);
		    	month = convertMonth(strMonth);setMonth(month);
		    	year = s1*100 + s2;setYear(year);
		    	ck = this.checkAllDateInput();
	    	}
  	}while(ck == 0);
  }
  
  public void showDateOnScreen() {
  	if(this.getDate() == 1) System.out.println(this.getDate()+"st/"+this.getMonth()+"/"+this.getYear());
  	else if(this.getDate() == 2) System.out.println(this.getDate()+"nd/"+this.getMonth()+"/"+this.getYear());
  	else if(this.getDate() == 3) System.out.println(this.getDate()+"rd/"+this.getMonth()+"/"+this.getYear());
  	else System.out.println(this.getDate()+"th/"+this.getMonth()+"/"+this.getYear());
  }
      
  public int convertDate(String str) {
  	int date = 0;
  	switch (str) {
		case "first": date =  1;break;
		case "second": date =  2;break;
      case "third": date = 3;break;
		case "forth":  date = 4; break;
		case "fifth":  date = 5; break;
		case "sixth":  date = 6; break;
		case "seventh":  date = 7; break;
		case "eighth":  date = 8; break;
		case "ninth":  date = 9; break;
		case "tenth":  date = 10; break;
		case "eleventh":  date = 11; break;
		case "twelfth":  date = 12; break;
		case "thirteenth":  date = 13; break;
		case "fourteenth":  date = 14; break;
		case "fifteenth":  date = 15; break;
		case "sixteenth":  date = 16; break;
		case "seventeenth":  date = 17; break;
		case "eighteenth":  date = 18; break;
		case "nineteenth":  date = 19; break;
		case "twenty":  date = 20; break;
		case "twenty-first":  date = 21; break;
		case "twenty-second":  date = 22; break;
		case "twenty-third":  date = 23; break;
		case "twenty-fourth": date = 24;break;
		case "twenty-fifth" : date = 25;break;
		case "twenty-sixth":date = 26;break;
		case "twenty-seventh":date = 27;break;
		case "twenty-eighth": date = 28;break;
		case "twenty-ninth":date = 29;break;
		case "thirtieth":date = 30;break;
		case "thirty-first":date = 31;break;
		default:
			date = 0;
			break;
		}
		return date;
  }
  
  public int convertMonth(String str) {
  	int month = 0;
  	switch(str){
  	case "january": month = 1; break;
		case "february": month = 2; break;
		case "march": month = 3;break;
		case "april":  month = 4; break;
		case "may":  month = 5; break;
		case "june":  month = 6; break;
		case "junly":  month = 7; break;
		case "august":  month = 8; break;
		case "september":  month = 9; break;
		case "october":  month = 10; break;
		case "november":  month = 11; break;
		case "decemver":  month = 12; break;
		default:
			month = 0;
			break;
		}
  	return month;
  }
  
  public int convertYear(String str) {
  	int year = 0;
  	switch(str){
  	case "zero" : year = 0;break;
  	case "one": year = 1; break;
		case "two": year = 2; break;
		case "three": year = 3;break;
		case "four":  year = 4; break;
		case "five":  year = 5; break;
		case "six":  year = 6; break;
		case "seven":  year = 7; break;
		case "eight":  year = 8; break;
		case "nine":  year = 9; break;
		case "ten":  year = 10; break;
		case "eleven":  year = 11; break;
		case "twelf":  year = 12; break;
		case "thirdteen":  year = 13; break;
		case "fourteen":  year = 14; break;
		case "fifteen":  year = 15; break;
		case "sixteen":  year = 16; break;
		case "seventeen":  year = 17; break;
		case "eightteen":  year = 18; break;
		case "nineteen":  year = 19; break;
		case "twenty":  year = 20; break;
		default:
			year = 10000;
			break;
		}
  	return year;
  }
  
  public String convertString(int a) {
  	String str = new String();
  	switch(a) {
  	case 1: str = "january";break;
  	case 2: str = "febraury";break;
  	case 3: str = "march"; break;
  	case 5: str = "may"; break;
  	case 6: str = "june";break;
  	case 7: str = "july";break;
  	case 8: str = "august"; break;
  	case 9: str = "september"; break;
  	case 10: str = "october";break;
  	case 11: str = "november";break;
  	case 12: str = "december"; break;
  	case 4: str = "april"; break;
  	default: str = "";break;
  	}
  	return str;
  }
  
//	public static void main(String[] args) {
//		MyDate abc = new MyDate();
//		abc.GetDateFromUser();
//		abc.showDateOnScreen();
//	}
	
}
